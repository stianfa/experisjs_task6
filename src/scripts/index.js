import '../styles/index.scss';
import 'jquery';

const apiKey = "563492ad6f91700001000001cfdd45d0a43f4726b968aa0846662dd1";

let searchText = "Landscape";
let numOfResults = 10;

let apiUrl = `https://api.pexels.com/v1/search?query=${searchText}&per_page=15&page=1`;


// Add html-rows for the photos
function renderPhotos(photos) {
    $.each(photos, (i, photo)=>{
        document.getElementById("row").innerHTML += createCard(photo);
    });
};

// Create one photo-card
function createCard(photo) {
    return `
    <div class="col-4">
        <div class="card " style="18rem;">
            <div class="card-img rounded float-left">
                <img class="card-img-top" src="${photo.src.portrait}"/>
            </div>
                
            <div class="card-body">
                <h6>${photo.photographer}</h6>
            </div>
        </div>
    </div>
    `;
   }

// Collect values for the search 
$(document).ready(function() {
    callAjax();
    $('#searchBtn').click(function(){
        submitClick();
    });
    $(document).on('keypress', function(e) {
        if(e.which == 13) {
            submitClick();
        }
    });
});


// Make GET-request
function callAjax() {
    $.ajax({
        url: `${apiUrl}`,
        type: 'GET',
        headers: {
            'Authorization': `Bearer ${apiKey}`
        }
    }).done(data => {
            renderPhotos(data.photos);
        });
}

function submitClick() {
    searchText = $("#searchField").val();
    numOfResults = $("#selectNum").val();
    apiUrl =  `https://api.pexels.com/v1/search?query=${searchText}&per_page=${numOfResults}&page=1`;
    $("#row").empty();
    callAjax();
}
